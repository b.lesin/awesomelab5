<html>
<head>
    <meta charset="utf-8"/>
    <title>Форма с базой данных</title>
    <link rel="stylesheet" media="all" href="style.css"/>
    <style>
        .error {
            border: 2px solid red;
        }
        .error-radio{
            border: 2px solid red;
            width: 100px;
        }
        .error-checkbox {
            border: 2px solid red;
            width: 350px;
        }
    </style>

</head>
<body>
<?php
if (!empty($messages)) {
    print('<div id="messages">');

    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}
?>
<form action="" id="my-formcarry" accept-charset="UTF-8" class="main" method="POST">
    <input id="name" type="text" name="fio" placeholder="Введите имя" <?php if ($errors['fio'] || $errors['fio_data']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"/>
    <input style="margin-bottom : 1em;margin-top : 1em" id="formmail" type="email" name="email"
        <?php if ($errors['email'] || $errors['email_data']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" placeholder="Введите почту"/>
    <label>
        Выберите свою дату рождения:<br/>
        <input id="dr" name="birthday"  type="date"
        <?php if($errors['birthday'] || $errors['birthday_data']) {print 'class = "error"';}?> value="<?php print $values['birthday']; ?>" />
    </label><br/>

    Выберите пол:<br/>
    <label <?php if($errors['gender']) { print 'class = "error-radio"';} ?>>
        <input type="radio" name="radio2" value="man"
        <?php if($values['gender']=="man") {print 'checked';}?>/>
        Мужской</label>
    <label <?php if($errors['gender']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="radio2" value="woman"
            <?php if($values['gender']=="woman") {print 'checked';}?>/>
        Женский</label>
    <br/>

    Сколько у вас конечностей :<br/>
    <label <?php if($errors['limbs']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="radio1" value="1"
            <?php if($values['limbs']=="1") {print 'checked';}?>/>
        1</label>
    <label <?php if($errors['limbs']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="radio1" value="2"
            <?php if($values['limbs']=="2") {print 'checked';}?>/>
        2</label>
    <label <?php if($errors['limbs']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="radio1" value="3"
            <?php if($values['limbs']=="3") {print 'checked';}?>/>
        3</label>
    <label <?php if($errors['limbs']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="radio1" value="4"
            <?php if($values['limbs']=="4") {print 'checked';}?>/>
        4</label>
    <label <?php if($errors['limbs']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="radio1" value="5"
            <?php if($values['limbs']=="5") {print 'checked';}?>/>
        5</label><br>

    <label>
        Ваши сверхспособности:
        <br/>
        <select id="sup_power" name="select1[]"
                multiple="multiple">
            <option value='imm'>Бессмертие</option>
            <option value='ph'>Прохожение сквозь стены</option>
            <option value='lv'>Левитация</option>

        </select>
    </label><br/>

     <label>
        Биография: <br/>
        <textarea id="bio" name="textarea1" placeholder="Пиши тут"<?php if ($errors['bio']) {print 'class="error"';} ?>><?php print $values['bio'];?></textarea>
    </label><br/>

    <label <?php if($errors['check']) { print 'class = "error-checkbox"';} ?>><input style="margin-bottom : 1em;margin-top : 1em;" id="formcheck" type="checkbox" name="checkbox" value="1"
            <?php if($values['check']=="1") {print 'checked';}?>/>Согласие на обработку персональных данных</label>

    <input type="submit" style="margin-bottom : -1em" id="formsend" class="buttonform" value="Отправить">
</form>
</body>
</html>
