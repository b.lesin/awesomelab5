<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию..
session_start();
// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
    // Если есть логин в сессии, то пользователь уже авторизован.
    // TODO: Сделать выход (окончание сессии вызовом session_destroy()

    session_destroy();
    header('Location:index.php');
    //при нажатии на кнопку Выход).
    // Делаем перенаправление на форму.
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if(!empty($_SESSION['error_session'])){
        $msg = $_SESSION['error_session'];
        print("<div>'$msg'</div>");
        $_SESSION['error_session']="";
    }
    ?>
    <form action="login.php" method="post">
        <input name="login" />
        <input name="pass" />
        <input type="submit" value="Войти" />
    </form>

    <?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

    // TODO: Проверть есть ли такой логин и пароль в базе данных.
    // Выдать сообщение об ошибках.
    include 'db_info.php';

    $sth = $db -> prepare("SELECT id,login,pass FROM form1");
    $sth->execute();
    $r = $db ->query("SELECT COUNT(*) FROM form1");
    $count = $r ->fetchColumn();//Количество записей в Базе данных
    $flag=0;
    $some_data = $sth->fetchAll();
    for($i=0;$i<$count;$i++)
    {
        if($some_data[$i]['login']==$_POST['login']){
            $flag=1;
            if(password_verify(password_hash($_POST['pass'],PASSWORD_DEFAULT),$some_data[$i]['pass'])){
                $_SESSION['error_session']="";
                $id= $some_data[$i]['id'];
                break;//All good
            }else{
                $_SESSION['error_session'] = "Bad pass";
                header('Location:login.php');
                exit();
            }
        }
    }
    if($flag==0){
        $_SESSION['error_session'] = "Bad login";
        header('Location:login.php');
        exit();
    }
    // Если все ок, то авторизуем пользователя.
    $_SESSION['login'] = $_POST['login'];
    // Записываем ID пользователя.
    $_SESSION['uid'] = $id;
    // Делаем перенаправление.
    header('Location: index.php');
}
