<?php
header('Content-Type: text/html; charset=UTF-8');
include 'db_info.php';
$values = array();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();

    if(!empty($_COOKIE['save'])) {
        setcookie('save', '',100000);
        setcookie('login','',100000);
        setcookie('pass','',100000);
        $messages[]= 'Спасибо, результаты сохранены.';

        if(!empty($_COOKIE['pass'])){
            $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином 
<strong>%s</strong> и паролем <strong>%s</strong> для измнения данных.',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
        }
    }
    if(!empty($_SESSION['login'])){
        print '<div>Im here</div>';
    }
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['fio_data'] = !empty($_COOKIE['fio_data_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['email_data'] = !empty($_COOKIE['email_data_error']);
    $errors['birthday'] = !empty($_COOKIE['bd_error']);
    $errors['birthday_data'] = !empty($_COOKIE['bd_data_error']);
    $errors['gender'] = !empty($_COOKIE['gen_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);


    if($errors['fio']) {
        setcookie('fio_error','',100000);
        $messages[] = '<div> Заполните имя</div>';
    }
    if($errors['fio_data']){
        setcookie('fio_data_error','',100000);
        $messages[] = '<div>Разрешено использовать только буквы русского языка(Прописные и строчные)';
    }
    if($errors['email']) {
        setcookie('email_error','',100000);
        $messages[]='<div>Введите почту</div>';
    }
    if($errors['email_data']){
        setcookie('email_data_error','',100000);
        $messages[]='<div>Введите корректную почту</div>';
    }
    if($errors['birthday']) {
        setcookie('bd_error','',100000);
        $messages[]='<div>Введите корректную дату рождения</div>';
    }
    if($errors['birthday_data']){
        setcookie('bd_data_error','',100000);
        $messages[]='<div>Вы не можете быть из будущего</div>';
    }
    if($errors['gender']) {
        setcookie('gen_error','',100000);
        $messages[]='<div>Выберите свой пол</div>';
    }
    if($errors['limbs'])
    {
        setcookie('limbs_error','',100000);
        $messages[] = '<div>Выберите количество конечностей</div>';
    }
    if($errors['bio']) {
        setcookie('bio_error','',100000);
        $messages[]='<div>Расскажите о себе</div>';
    }
    if($errors['check']) {
        setcookie('check_error','',100000);
        $messages[] = '<div>Подтвердите согласие на обработку персональных данных</div>';
    }

    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['birthday'] = empty($_COOKIE['bd_value']) ? '' : $_COOKIE['bd_value'];
    $values['gender'] = empty($_COOKIE['gen_value']) ? '' : strip_tags($_COOKIE['gen_value']);
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
    $values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
    $flag = 1;
    foreach($errors as $err){ //Проверка ошибок
        if($err==1)$flag=0;
    }
    if($flag==1 && !empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])){
        // TODO: загрузить данные пользователя из БД
        // и заполнить переменную $values,
        // предварительно санитизовав.
        $lg = $_SESSION['login'];
        $request = "SELECT name,email,year,sex,lb,bio,checkbox FROM form1 WHERE login = '$lg'";
        $result = $db -> prepare($request);
        $result ->execute();

        $data = $result->fetch(PDO::FETCH_ASSOC);

        $values['fio'] = strip_tags($data['name']);
        $values['email'] = strip_tags($data['email']);
        $values['birthday'] = strip_tags($data['year']);
        $values['gender'] = strip_tags($data['sex']);
        $values['limbs'] = $data['lb'];
        $values['bio'] = strip_tags($data['bio']);
        $values['check'] = strip_tags($data['checkbox']);
        printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
        print '<br/><a href="login.php">Нажмите</a> для выхода';
    }else{
       print '<br/><a href="login.php">Нажмите</a> для того чтобы авторизоваться';
    }
    include('form.php');
}

else {
    $errors = FALSE;
    if(empty($_POST['fio'])) {
        setcookie('fio_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    elseif (!preg_match('/[а-яёА-ЯЁ]{1,}+$/', $_POST['fio'])) {
        setcookie('fio_data_error','1',time()+24*60*60);
        $errors=TRUE;
    }
    else {
        setcookie('fio_value',$_POST['fio'],time()+365*24*60*60);
    }

    if(empty($_POST['email'])) {
        setcookie('email_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    elseif(!preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u',$_POST['email'])){
        setcookie('email_data_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('email_value',$_POST['email'],time()+365*24*60*60);
    }
    if(empty($_POST['birthday'])) {
        setcookie('bd_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    elseif($_POST['birthday']>date("Y-m-d")){
        setcookie('bd_data_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('bd_value',$_POST['birthday'],time()+365*24*60*60);
    }


    if(empty($_POST['radio2'])) {
        setcookie('gen_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('gen_value',$_POST['radio2'],time()+365*24*60*60);
    }
    if(empty($_POST['radio1'])) {
        setcookie('limbs_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else{
        setcookie('limbs_value',$_POST['radio1'],time()+365*24*60*60);
    }
    if(empty($_POST['textarea1'])) {
        setcookie('bio_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else{
        setcookie('bio_value',$_POST['textarea1'],time()+365*24*60*60);
    }
    if(empty($_POST['checkbox'])) {
        setcookie('check_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('check_value',$_POST['checkbox'],time()+365*24*60*60);
    }

    if($errors) {
        header('Location:index.php');
        exit();
    }
    else {
        setcookie('fio_error', '', 100000);
        setcookie('fio_data_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('email_data_error','',time()+100000);
        setcookie('bd_error','',100000);
        setcookie('bd_data_error','',100000);
        setcookie('gen_error','',100000);
        setcookie('limbs_error','',100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error','',100000);
    }

    if(!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
        //Rewrite data
       $name = $_POST['fio'];
       $email = $_POST['email'];
       $birthday = $_POST['birthday'];
       $gender = $_POST['radio2'];
       $limbs = $_POST['radio1'];
       $bio = $_POST['textarea1'] ;
       $lg = $_SESSION['login'];

       $sql = "UPDATE form2 SET name='$name',email='$email',year='$birthday',sex='$gender',lb='$limbs',bio='$bio' WHERE login='$lg'";
       $stmt = $db->prepare($sql);
       $stmt->execute();

       $r = "SELECT id from form2 WHERE login='$lg'";
       $s = $db->prepare($r);
       $s->execute();
       $data_id = $s->fetch(PDO::FETCH_ASSOC);
       $id = $data_id['id'];
       $abil = $_POST['select1'];
        $abil_info = array('imm', 'ph', 'lv');
        $data = array(
            'imm' => 0,
            'ph' => 0,
            'lv' => 0,
        );
        foreach ($abil as $ab) {
            if (in_array($ab, $abil_info)) {
                $data[$ab] = 1;
            }
        }
        $imm = $data['imm'];
        $ph = $data['ph'];
        $lv = $data['lv'];
       $sql = "UPDATE abil2 SET immortal='$imm',phasing='$ph',levitation='$lv' WHERE id='$id'";
       $stmt = $db->prepare($sql);
       $stmt->execute();
    }
    else{
        $login = uniqid();
        $pass = strval(rand());

        setcookie('login',$login);
        setcookie('pass',$pass);
        $pass_for_bd = password_hash($pass,PASSWORD_DEFAULT);
        $stmt = $db->prepare("INSERT INTO form2 (name, email,year,sex,lb,bio,checkbox,login,pass) VALUES (:fio, :email,:birthday,:sex,:lb,:bio,:checkbox,:login,:pass)");
        $stmt -> execute(array('fio'=>$_POST['fio'], 'email'=>$_POST['email'],'birthday'=>$_POST['birthday'],'sex'=>$_POST['radio2'],'lb'=>$_POST['radio1'],'bio'=>$_POST['textarea1'],'checkbox'=>$_POST['checkbox'],'login' =>$login, 'pass' => $pass_for_bd));

        $stmt = $db->prepare("SELECT id from form2 where login='$login'");
        $stmt->execute();
        $id1 = $stmt->fetch();
        $id = $id1[0];

        $stmt = $db->prepare("INSERT INTO abil2(id,immortal,phasing,levitation) VALUES(:id,:imm,:ph,:lv)");
        $abil = $_POST['select1'];
        $abil_info = array('imm', 'ph', 'lv');
        $data = array(
            'imm' => 0,
            'ph' => 0,
            'lv' => 0,
        );
        foreach ($abil as $ab) {
            if (in_array($ab, $abil_info)) {
                $data[$ab] = 1;
            }
        }
        $imm = $data['imm'];
        $ph = $data['ph'];
        $lv = $data['lv'];
        $stmt->execute(array('id' => $id, 'imm' => $data['imm'], 'ph' => $data['ph'], 'lv' => $data['lv']));
    }
    setcookie('save', '1');

    header('Location: index.php');

}
